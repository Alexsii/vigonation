/*Stars *************************************/
$('#number_stars').ready(function () {
    let number = $('#number_stars').val(), i = parseInt(0);

    if (number > 0 && number < 6) {
        for (i; i < number; i++) {
            i = i + 1;
            $('#stars_' + i).addClass('starts_active');
            i = i - 1;
        }
    } else {
        return false;
    }
});
/*end******************************************/
/* Description   *********************************/
$('#description').click(function () {
    $('#description').addClass('active');
    $('#ingredients').removeClass('active');
    $('#intake').removeClass('active');

    $('#description_1').addClass('active');
    $('#ingredients_1').removeClass('active');
    $('#intake_1').removeClass('active');
});

$('#ingredients').click(function () {
    $('#description').removeClass('active');
    $('#ingredients').addClass('active');
    $('#intake').removeClass('active');

    $('#description_1').removeClass('active');
    $('#ingredients_1').addClass('active');
    $('#intake_1').removeClass('active');
});
$('#intake').click(function () {
    $('#description').removeClass('active');
    $('#ingredients').removeClass('active');
    $('#intake').addClass('active');

    $('#description_1').removeClass('active');
    $('#ingredients_1').removeClass('active');
    $('#intake_1').addClass('active');
});
/****end******************************************/
/*quantity ****************************************/
const price_figure = $('#price_figure').text();

$('#minus').click(function () {
    let price_figure_result = $('#price_figure').text(), point = $('#point').text();

    if (point > 1) {
        $('#price_figure').text(parseInt(price_figure_result) - price_figure);
        $('#point').text(parseInt(point) - 1);
    } else {
        return false;
    }
});

$('#plus').click(function () {
    let price_figure_result = $('#price_figure').text(),
        point = $('#point').text();

    $('#price_figure').text(parseInt(price_figure_result) + parseInt(price_figure));
    $('#point').text(parseInt(point) + 1);

});
/*end**********************************************/
/*slider**************************************************/
$('#slider').mousedown(function (event) {
    let mouse = event.pageY,
        slider_hight = parseInt($('#slider').css('margin-top')),
        img_slider_height = parseInt($('#image_slider').css('height'));
    $(window).mousemove(function (event) {
        $('html').mouseup(function () {
            $(window).off('mousemove');
        });
        let mouse_result = parseInt(mouse - event.pageY),
            slider = slider_hight - mouse_result,
            slider_img = img_slider_height - mouse_result;
        if (slider > -20 && slider < 532) {
            $('#slider').css('margin-top', slider);
            $('#image_slider').css('height', slider_img);
        }
    });
});

/*PARALLAX*********************************************************************************/
$(window).scroll(function () {
    let st = parseInt($(this).scrollTop());
    if (st < 4122) {
        $('.parallax_box img').css({
            'transform': 'translate(0%, -' + st / 45 + '%)'
        });
    }
    if (st < 4222) {
        $('.parallax_box p').css({
            'transform': 'translate(0%,' + st / 2 + '%)'
        });
    }
});
/*end*/
/*Loading **************************************************************************************/

$(function () {
    let $elie = $("#loading_img"),
        degree = 0, timer;

    rotate();
    function rotate() {
        $elie.css({WebkitTransform: 'rotate(-' + degree + 'deg)'});
        $elie.css({'-moz-transform': 'rotate(-' + degree + 'deg)'});
        timer = setTimeout(function () {
            ++degree;
            // rotate();
        }, 6);
        if (degree > 6666) {
            degree = 0;
        }
    }
});
let state = true;
/*drop out menu*/
$('.header_menu_mobile').click(function () {
    if( state  === true){
        $('.mobile_drop_menu').css('display', 'block');
        $('.mobile_drop_menu').css('visibility', 'visible');
        state = !state;
    }else{
        $('.mobile_drop_menu').css('display', 'none');
        $('.mobile_drop_menu').css('visibility', 'hidden');
        state = !state;
    }
});